/*
 * log.c
 *
 *  Created on: 26 janv. 2019
 *      Author: Luca Mayer-Dalverny
 */

/*Standard C include*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*Log header include*/
#include "log.h"

/*ANSI colors definitions*/
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_CYAN	   "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

static log_status get_set_log_status(bool is_updated, log_status status_update)
{
	static log_status status = LOG_NONE;

	if(is_updated)
	{
		status = status_update;
	}

	return status;
}

void log_display(log_status new_status)
{
	get_set_log_status(true,new_status);
}

void log_debug(char* message)
{
	log_status status = get_set_log_status(false,LOG_NONE);

	if((status == LOG_ALL)||(status == LOG_DEBUG)||(status == (LOG_DEBUG+LOG_INFO))||(status == (LOG_DEBUG+LOG_WARNING))||(status == (LOG_DEBUG+LOG_INFO+LOG_WARNING))||(status == (LOG_DEBUG+LOG_ERROR))||(status == (LOG_DEBUG+LOG_INFO+LOG_ERROR))||(status == (LOG_DEBUG+LOG_WARNING+LOG_ERROR)))
	{
		/*Changing the prompt color to magenta*/
		printf(ANSI_COLOR_CYAN);
		/*Printing the type of the message*/
		printf("[DEBUG] ");
		/*Printing the message*/
		printf("%s",message);
		/*Reseting the prompt color*/
		printf(ANSI_COLOR_RESET);
		/*End of the line*/
		printf("\n");
	}
}

void log_info(char* message)
{
	log_status status = get_set_log_status(false,LOG_NONE);

	if((status == LOG_ALL)||(status == LOG_INFO)||(status == (LOG_INFO+LOG_DEBUG))||(status == (LOG_INFO+LOG_WARNING))||(status == (LOG_INFO+LOG_DEBUG+LOG_WARNING))||(status == (LOG_INFO+LOG_ERROR))||(status == (LOG_INFO+LOG_DEBUG+LOG_WARNING))||(status == (LOG_INFO+LOG_WARNING+LOG_ERROR)))
	{
		/*Changing the prompt color to blue*/
		printf(ANSI_COLOR_GREEN);
		/*Printing the type of the message*/
		printf("[INFO] ");
		/*Printing the message*/
		printf("%s",message);
		/*Reseting the prompt color*/
		printf(ANSI_COLOR_RESET);
		/*End of the line*/
		printf("\n");
	}
}

void log_warning(char* message)
{
	log_status status = get_set_log_status(false,LOG_NONE);

	if((status == LOG_ALL)||(status == LOG_WARNING)||(status == (LOG_WARNING+LOG_DEBUG))||(status == (LOG_WARNING+LOG_INFO))||(status == (LOG_WARNING+LOG_DEBUG+LOG_INFO))||(status == (LOG_WARNING+LOG_ERROR))||(status == (LOG_WARNING+LOG_DEBUG+LOG_ERROR))||(status == (LOG_WARNING+LOG_INFO+LOG_ERROR)))
	{
		/*Changing the prompt color to yellow*/
		printf(ANSI_COLOR_YELLOW);
		/*Printing the type of the message*/
		printf("[WARNING] ");
		/*Printing the message*/
		printf("%s",message);
		/*Reseting the prompt color*/
		printf(ANSI_COLOR_RESET);
		/*End of the line*/
		printf("\n");
	}
}

void log_error(char* message)
{
	log_status status = get_set_log_status(false,LOG_NONE);

	if((status == LOG_ALL)||(status == LOG_ERROR)||(status == (LOG_ERROR+LOG_DEBUG))||(status == (LOG_ERROR+LOG_INFO))||(status == (LOG_ERROR+LOG_INFO+LOG_DEBUG))||(status == (LOG_ERROR+LOG_WARNING))||(status == (LOG_ERROR+LOG_DEBUG+LOG_WARNING))||(status == (LOG_ERROR+LOG_INFO+LOG_WARNING)))
	{
		/*Changing the prompt color to red*/
		printf(ANSI_COLOR_RED);
		/*Printing the type of the message*/
		printf("[ERROR] ");
		/*Printing the message*/
		printf("%s",message);
		/*Reseting the prompt color*/
		printf(ANSI_COLOR_RESET);
		/*End of the line*/
		printf("\n");
	}
}
