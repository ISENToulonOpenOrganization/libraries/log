/*
 * main_log.c
 *
 *  Created on: 26 janv. 2019
 *      Author: Luca Mayer-Dalverny
 */

#include <stdio.h>
#include <stdlib.h>

#include "log.h"

int main(void)
{
	printf("All\n");
	log_display(LOG_ALL);
	log_debug("This is a debug");
	log_info("This is an info");
	log_warning("This is a warning");
	log_error("This is an error");

	printf("Info + Error\n");
	log_display(LOG_INFO+LOG_ERROR);
	log_debug("This is a debug");
	log_info("This is an info");
	log_warning("This is a warning");
	log_error("This is an error");

	printf("Debug + Error + Warning\n");
	log_display(LOG_DEBUG+LOG_ERROR+LOG_WARNING);
	log_debug("This is a debug");
	log_info("This is an info");
	log_warning("This is a warning");
	log_error("This is an error");
}
