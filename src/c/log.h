/**
 * @title log.h
 * @date 26 janv. 2019
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef LOG_H_
#define LOG_H_

/**
 * @chap Contenu de log.h
 */

/**
 * @sec Le paramétrage des logs
 */

/**
 * @enum log_status
 * @desc représente les différent type de log que l'on peut afficher dans la console.
 * @bitem
 * @item LOG_NONE : n'affiche aucun log
 * @item LOG_DEBUG : affiche uniquement les logs pour le débogage
 * @item LOG_INFO : affiche uniquement les logs d'information
 * @item LOG_WARNING : affiche uniquement les logs d'avertissement
 * @item LOG_ERROR : affiche uniquement les logs d'erreur
 * @item LOG_ALL : affiche tous les logs
 * @eitem
 */
typedef enum {LOG_NONE = 0, LOG_DEBUG = 2, LOG_INFO = 4, LOG_WARNING = 8, LOG_ERROR = 16, LOG_ALL = 30}log_status;

/**
 * @fn log_display
 * @desc Défini le type de log à afficher en console. Il est possible de faire des combinaisons linéaires pour afficher plusieurs type de logs (ex. LOG_INFO+LOG_ERROR).
 * @param new_status (log_status) : le type de log à affiche
 * @bcode
 */
void log_display(log_status new_status);
/**
 * @ecode
 */

/**
 * @sec Les fonctions d'affichage des logs
 */

/**
 * @fn log_debug
 * @desc Affiche un message pour débugger (de couleur cyan) dans la console.
 * @param message (char*) : le message à afficher
 * @bcode
 */
void log_debug(char* message);
/**
 * @ecode
 */

/**
 * @fn log_info
 * @desc Affiche un message d'information (de couleur verte) dans la console.
 * @param message (char*) : le message à afficher
 * @bcode
 */
void log_info(char* message);
/**
 * @ecode
 */

/**
 * @fn log_warning
 * @desc Affiche un message d'avertissement (de couleur jaune) dans la console.
 * @param message (char*) : le message à afficher
 * @bcode
 */
void log_warning(char* message);
/**
 * @ecode
 */

/**
 * @fn error
 * @desc Affiche un message d'erreur (de couleur rouge) dans la console.
 * @param message (char*) : le message à afficher
 * @bcode
 */
void log_error(char* message);
/**
 * @ecode
 */

#endif /* LOG_H_ */
